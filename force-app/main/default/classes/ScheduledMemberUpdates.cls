/**
 * @description       : 
 * @author            : Prashanth
 * @group             : 
 * @last modified on  : 11-24-2021
 * @last modified by  : Prashanth 
 * Modifications Log
 * Ver   Date         Author      Modification
 * 1.0   11-24-2021   Prashanth   Initial Version
**/
global without sharing class ScheduledMemberUpdates implements Schedulable {
    global void execute(SchedulableContext sc) {
        UpdateMemberAccounts memAcc = new UpdateMemberAccounts();
        Database.executeBatch(memAcc, 200);
    }
}