/**
 * @description       : 
 * @author            : Prashanth
 * @group             : 
 * @last modified on  : 12-01-2021
 * @last modified by  : Prashanth
 * Modifications Log
 * Ver   Date         Author      Modification
 * 1.0   11-23-2021   Prashanth   Initial Version
**/
public without sharing class PrepareInvoicesForEmail {
    @InvocableMethod
    public static void prepInvoicesForEmail(List<requestWrapper> requests){
        if(!requests[0].prepareVetInvoices){
            List<Employer_Invoice__C> empInvs = [SELECT Id FROM Employer_Invoice__C WHERE Invoice_Email_Sent__c = FALSE AND Employer_Email__c != NULL AND Employer__r.Exempt_from_Billing_Emails__c = false];
            for(Employer_Invoice__C empInv : empInvs){
                empInv.Send_Email__c = true;
            }
            if(!empInvs.isEmpty()){
                update empInvs;
            }
        }
        else if(requests[0].prepareVetInvoices){
            List<Vet_Payments__c> vetPays = [SELECT Id FROM Vet_Payments__c WHERE Invoice_Email_Sent__c = FALSE AND Vet_Email__c != NULL AND Vet__r.Exempt_from_Billing_Emails__c = false];
            for(Vet_Payments__c vetPay : vetPays){
                vetPay.Send_Email__c = true;
            }
            if(!vetPays.isEmpty()){
                update vetPays;
            }
        }
    }

    public class requestWrapper{
        @InvocableVariable
        public Boolean prepareVetInvoices = true;
    }
}