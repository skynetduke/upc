/**
 * @description       : Test Class for GenerateVetPayments class
 * @author            : Prashanth
 * @group             : 
 * @last modified on  : 11-18-2021
 * @last modified by  : Prashanth
 * Modifications Log
 * Ver   Date         Author      Modification
 * 1.0   11-09-2021   Prashanth   Initial Version
**/

@isTest
public with sharing class GenerateVetPaymentsTest {
    @TestSetup
    static void makeData(){
        String vetRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Vet').getRecordTypeId();
        Account vet1 = new Account(Name = 'Vet 1', Number_Of_Pets_Calculate__c = 100, RecordTypeId = vetRecTypeId);
        Account vet2 = new Account(Name = 'Vet 2', RecordTypeId = vetRecTypeId);

        insert new List<Account>{vet1, vet2};
    }

    @isTest
    public static void doTest(){
        Test.startTest();
        GenerateVetPayments.generateVetPayments(null);
        System.assertEquals([SELECT ID FROM Vet_Payments__c ].size(), 1);
        Test.stopTest();
    }
}