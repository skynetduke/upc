/**
 * @description       : 
 * @author            : Prashanth
 * @group             : 
 * @last modified on  : 11-24-2021
 * @last modified by  : Prashanth
 * Modifications Log
 * Ver   Date         Author      Modification
 * 1.0   11-23-2021   Prashanth   Initial Version
**/
public without sharing class UpdateMemberAccounts implements Database.Batchable<sObject>{
    
    public Database.querylocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id FROM Account WHERE RecordType.DeveloperName = \'Member\' AND (Employer__c <> null OR Current_Vet__c <> null)');
    }
    
    public void execute(Database.BatchableContext BC, List<sObject> scope){
        Set<Account> members = new Set<Account>();
        List<AggregateResult> agrs = [SELECT Max(Id) id FROM Account WHERE ID in :scope AND RecordType.DeveloperName = 'Member' GROUP BY Employer__c];
        for(AggregateResult agr : agrs){
            String memId = String.valueOf(agr.get('id'));
            if(String.isNotBlank(memId)){
                members.add(new Account(Id = memId));
            }
        }

        List<AggregateResult> agrs1 = [SELECT Max(Id) id FROM Account WHERE ID in :scope AND RecordType.DeveloperName = 'Member' GROUP BY Current_Vet__c];
        for(AggregateResult agr : agrs1){
            String memId = String.valueOf(agr.get('id'));
            if(String.isNotBlank(memId)){
                members.add(new Account(Id = memId));
            }
        }

        List<Account> memberList = new List<Account>();
        memberList.addAll(members);

        if(!memberList.isEmpty()){
            update memberList;
        }    
        
    }
    
    public void finish(Database.BatchableContext BC){
    
    }
    
}