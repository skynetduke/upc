/**
 * @description       : Handler class to handle business logic
 * @author            : Prashanth
 * @group             : 
 * @last modified on  : 12-21-2021
 * @last modified by  : Prashanth
 * Modifications Log
 * Ver   Date         Author      Modification
 * 1.0   11-05-2021   Prashanth   Initial Version
**/
public without sharing class PetTriggerHandler {
    /**
    * @description Apex method to update number of pets for a vet
    * @author Prashanth | 11-05-2021 
    * @param Map<Id Pet__c> newMap 
    * @param Map<Id Pet__c> oldMap 
    **/
    public void calculateVetPets(Map<Id,Pet__c> oldMap, Map<Id,Pet__c> newMap){
        Set<Id> accountIdSet = new Set<Id>();
        //Using oldmap to reduce the calculated pets on the previous Vet
        if(oldMap != null && oldMap.size() > 0){
            for(Pet__c pet : oldMap.values()){
                if(String.isNotBlank(pet.Vet_Office__c)){
                    accountIdSet.add(pet.Vet_Office__c);
                }
            }
        }

        if(newMap != null && newMap.size() > 0){
            for(Pet__c pet : newMap.values()){
                if(String.isNotBlank(pet.Vet_Office__c)){
                    accountIdSet.add(pet.Vet_Office__c);
                }
            }
        }

        List<Account> accountsTobeUpdated = new List<Account>();
        //Update Accounts with new pet count
        List<Account> vetAccs = [SELECT Id, 
        (SELECT Id FROM Pets1__r WHERE Deceased__c = false AND Pet_Owner__r.Registered_Member__c = TRUE)
        FROM Account WHERE RecordType.DeveloperName = 'Vet' AND Id IN :accountIdSet];//Pet_Owner__r.Effective_Date__c = NEXT_MONTH OR
        for(Account acc : vetAccs){
            Integer count = 0;
            for(Pet__c pet : acc.Pets1__r){
                count++;
            }
            accountsTobeUpdated.add(new Account(Id = acc.Id, Number_Of_Pets_Calculate__c = count));
        }

        //Update vet accounts
        if(!accountsTobeUpdated.isEmpty()){
            update accountsTobeUpdated;
        }

    }
}