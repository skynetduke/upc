/**
 * @description       : 
 * @author            : Prashanth
 * @group             : 
 * @last modified on  : 11-24-2021
 * @last modified by  : Prashanth
 * Modifications Log
 * Ver   Date         Author      Modification
 * 1.0   11-24-2021   Prashanth   Initial Version
**/
@isTest
public with sharing class UpdateMemberAccountsTest {
    @TestSetup
    static void makeData(){
        String vetRecId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Vet').getRecordTypeId();
        String empRecId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Employer').getRecordTypeId();
        String memRecId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Member').getRecordTypeId();
        List<Account> accs = new List<Account>();
        
        accs.add(generateAcc('Employer 1', empRecId));
        
        accs.add(generateAcc('Employer 2', empRecId));
        
        accs.add(generateAcc('Vet 1', vetRecId));

        accs.add(generateAcc('Vet 2', vetRecId));

        insert accs;

        Account member1 = new Account(); 
        member1.FirstName = 'FName Mem 1';
        member1.LastName = 'LName Mem 1';
        member1.RecordTypeId = memRecId;
        member1.Employer__c = accs[0].Id;
        member1.Effective_Date__c = system.today();
        member1.Current_Vet__c = accs[2].Id;
        accs.add(member1);

        Account member2 = new Account(); 
        member2.FirstName = 'FName Mem 2';
        member2.LastName = 'LName Mem 2';
        member2.RecordTypeId = memRecId;
        member2.Employer__c = accs[0].Id;
        member2.Effective_Date__c = system.today().addDays(1);
        member2.Current_Vet__c = accs[2].Id;
        accs.add(member2);

        Account member3 = new Account(); 
        member3.FirstName = 'FName Mem 3';
        member3.LastName = 'LName Mem 3';
        member3.RecordTypeId = memRecId;
        member3.Employer__c = accs[1].Id;
        member3.Effective_Date__c = system.today();
        member3.Current_Vet__c = accs[3].Id;
        accs.add(member3);

        Account member4 = new Account(); 
        member4.FirstName = 'FName Mem 4';
        member4.LastName = 'LName Mem 4';
        member4.RecordTypeId = memRecId;
        member4.Employer__c = accs[1].Id;
        member4.Effective_Date__c = system.today().addDays(1);
        member4.Current_Vet__c = accs[3].Id;
        accs.add(member4);

        upsert accs Id;
    }

    /*@isTest
    public static void doTest(){
        Test.startTest();
        UpdateMemberAccounts memAcc = new UpdateMemberAccounts();
        //memAcc.updateMembers();
        Test.stopTest();
    }*/

    @isTest
    public static void doTest1(){
        Test.startTest();
        String CRON_EXP = '0 0 0 '+system.today().addDays(1).day()+' '+system.today().month()+' ? '+system.today().year();
        String jobId = system.schedule('UpdateMemberAccounts', CRON_EXP, new ScheduledMemberUpdates());
        Test.stopTest();
    }

    private static Account generateAcc(String Name, String recTypeId){ 
        Account acc = new Account(); 
        acc.Name = Name;
        acc.RecordTypeId = recTypeId;
        return acc;
    }
}