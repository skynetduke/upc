/**
 * @description       : 
 * @author            : Prashanth
 * @group             : 
 * @last modified on  : 11-23-2021
 * @last modified by  : Prashanth
 * Modifications Log
 * Ver   Date         Author      Modification
 * 1.0   11-23-2021   Prashanth   Initial Version
**/
@isTest
public class SendFilesAsAttachmentsTest {
    @TestSetup
    static void makeData(){
        String vetRecId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Vet').getRecordTypeId();
        String empRecId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Employer').getRecordTypeId();

        List<Account> accs = new List<Account>();
        accs.add( 
            new Account(
            Name = 'Employer 1',
            recordTypeId = empRecId,
            Total_Fee__c = 100,
            Billing_Email__c = 'Test@empTest.com'
            )
        );

        accs.add( 
            new Account(
            Name = 'Vet 1',
            recordTypeId = vetRecId,
            Number_Of_Pets_Calculate__c = 10,
            Vet_Email__c = 'Test@vetTest.com'
            )
        );
        insert accs;

        Employer_Invoice__c empInv = new Employer_Invoice__c();
        empInv.Name = 'Test Employer';
        empInv.Employer__c = accs[0].Id;
        insert empInv;

        ContentVersion cv1 = new ContentVersion(
        Title          = 'Test Blob',
        PathOnClient   = 'Pic.txt',
        VersionData    = Blob.valueOf('Test Content'),
        IsMajorVersion = true);
        insert cv1; 

        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        ContentDocumentLink cdl1 = new ContentDocumentLink();
        cdl1.LinkedEntityId = empInv.Id;
        cdl1.ContentDocumentId = documents[0].Id;
        cdl1.ShareType = 'V';
        cdl1.Visibility = 'AllUsers';
        insert cdl1;

        Vet_Payments__c vetPay = new Vet_Payments__c();
        vetPay.Vet__c = accs[1].Id;
        insert vetPay;

        ContentVersion cv2 = new ContentVersion(
        Title          = 'Test Vet Blob',
        PathOnClient   = 'Pic.txt',
        VersionData    = Blob.valueOf('Test Content'),
        IsMajorVersion = true);
        insert cv2; 

        List<ContentDocument> documents1 = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument WHERE Title = 'Test Vet Blob'];

        ContentDocumentLink cd2 = new ContentDocumentLink();
        cd2.LinkedEntityId = vetPay.Id;
        cd2.ContentDocumentId = documents1[0].Id;
        cd2.ShareType = 'V';
        cd2.Visibility = 'AllUsers';
        insert cd2;

        
    }

    @isTest
    public static void doTest(){
        SendFilesAsAttachments.requestWrapper req = new SendFilesAsAttachments.requestWrapper();
        req.sendVetEmail = false;
        req.empInvs = [SELECT ID FROM Employer_Invoice__c];

        SendFilesAsAttachments.requestWrapper req1 = new SendFilesAsAttachments.requestWrapper();
        req1.sendVetEmail = true;
        req1.vetPays = [SELECT ID FROM Vet_Payments__c];

        Test.startTest();
        SendFilesAsAttachments.prepareRecordIds(new List<SendFilesAsAttachments.requestWrapper>{req});
        SendFilesAsAttachments.prepareRecordIds(new List<SendFilesAsAttachments.requestWrapper>{req1});
        Test.stopTest();
    }
}