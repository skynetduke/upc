/**
 * @description       : 
 * @author            : Prashanth
 * @group             : 
 * @last modified on  : 11-23-2021
 * @last modified by  : Prashanth
 * Modifications Log
 * Ver   Date         Author      Modification
 * 1.0   11-23-2021   Prashanth   Initial Version
**/
@isTest
public with sharing class PrepareInvoicesForEmailTest {
    @TestSetup
    static void makeData(){
        String vetRecId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Vet').getRecordTypeId();
        String empRecId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Employer').getRecordTypeId();

        List<Account> accs = new List<Account>();
        accs.add( 
            new Account(
            Name = 'Employer 1',
            recordTypeId = empRecId,
            Total_Fee__c = 100,
            Billing_Email__c = 'Test@empTest.com'
            )
        );

        accs.add( 
            new Account(
            Name = 'Vet 1',
            recordTypeId = vetRecId,
            Number_Of_Pets_Calculate__c = 10,
            Vet_Email__c = 'Test@vetTest.com'
            )
        );
        insert accs;

        Employer_Invoice__c empInv = new Employer_Invoice__c();
        empInv.Name = 'Test Employer';
        empInv.Employer__c = accs[0].Id;
        insert empInv;

        Vet_Payments__c vetPay = new Vet_Payments__c();
        vetPay.Vet__c = accs[1].Id;
        insert vetPay;
    }

    @isTest
    public static void doTest(){
        PrepareInvoicesForEmail.requestWrapper req = new PrepareInvoicesForEmail.requestWrapper();
        req.prepareVetInvoices = false;

        PrepareInvoicesForEmail.requestWrapper req1 = new PrepareInvoicesForEmail.requestWrapper();
        req1.prepareVetInvoices = true;

        Test.startTest();
        PrepareInvoicesForEmail.prepInvoicesForEmail(new List<PrepareInvoicesForEmail.requestWrapper>{req});
        PrepareInvoicesForEmail.prepInvoicesForEmail(new List<PrepareInvoicesForEmail.requestWrapper>{req1});
        Test.stopTest();
    }
}