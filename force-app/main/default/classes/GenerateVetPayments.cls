/**
 * @description       : Triggered from a flow to generate Vet Payments.
 * @author            : Prashanth
 * @group             : 
 * @last modified on  : 11-18-2021
 * @last modified by  : Prashanth
 * Modifications Log
 * Ver   Date         Author      Modification
 * 1.0   11-05-2021   Prashanth   Initial Version
**/

public without sharing class GenerateVetPayments {
    /**
    * @description Generate Vet payments for all the Vets
    * @author Prashanth | 11-05-2021 
    **/
    @InvocableMethod
    public static void generateVetPayments(List<Date> invoiceDates){
        String recordTypeName = 'Vet';
        List<Account> vets = [SELECT Id, Per_Pet_Rate__c, Number_Of_Pets_Calculate__c, Vet_Amount_Payable__c FROM Account WHERE RecordType.DeveloperName = :recordTypeName AND Number_Of_Pets_Calculate__c > 0];
        List<Vet_Payments__c> VetPays = new List<Vet_Payments__c>();
        List<SDOC__SDJob__c> sdocJobs = new List<SDOC__SDJob__c>();

        for(Account vet : vets){
            Vet_Payments__c vetPay = new Vet_Payments__c();
            vetPay.Vet__c = vet.Id;
            vetPay.Amount_Payable__c = vet.Vet_Amount_Payable__c;
            vetpay.Payment_Date__c = (invoiceDates != null && !invoiceDates.isEmpty()) ? invoiceDates[0] : system.today();
            VetPays.add(VetPay);
        }

        //Generate Vet Payements
        if(!vetPays.isEmpty()){
            insert vetPays;
        }

        for(Vet_Payments__c vetPay : vetPays){
            //Sdoc Job
            if(!Test.isRunningTest()){
                sdocJobs.add(new SDOC__SDJob__c(SDOC__Oid__c = vetPay.Id,  SDOC__ObjAPIName__c = 'Vet_Payments__c', SDOC__Doclist__c = Label.VetPayementsRosterNames, SDOC__Start__c = true));
            }
        }

        //Generate Vet Roster
        if(!sdocJobs.isEmpty()){
            insert sdocJobs;
        }
    }
}