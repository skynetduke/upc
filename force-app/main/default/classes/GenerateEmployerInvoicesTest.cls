/**
 * @description       : Test Class for GenerateEmployerInvoices class
 * @author            : Prashanth
 * @group             : 
 * @last modified on  : 11-18-2021
 * @last modified by  : Prashanth
 * Modifications Log
 * Ver   Date         Author      Modification
 * 1.0   11-03-2021   Prashanth   Initial Version
**/
@isTest
public with sharing class GenerateEmployerInvoicesTest {
    @TestSetup
    static void makeData(){
        String empRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Employer').getRecordTypeId();
        Account Emp1 = new Account(Name = 'Employer 1', Total_Fee__c = 1000, RecordTypeId = empRecTypeId);
        Account Emp2 = new Account(Name = 'Employer 2', RecordTypeId = empRecTypeId);

        insert new List<Account>{Emp1, Emp2};
    }

    @isTest
    public static void doTest(){
        Test.startTest();
        GenerateEmployerInvoices.generateInvoices(null);
        System.assertEquals([SELECT ID FROM Employer_Invoice__c].size(), 1);
        Test.stopTest();
    }
}