/**
 * @description       : Class that sends files of a list records as attachments.
 * @author            : Prashanth
 * @group             : 
 * @last modified on  : 11-23-2021
 * @last modified by  : Prashanth
 * Modifications Log
 * Ver   Date         Author      Modification
 * 1.0   11-16-2021   Prashanth   Initial Version
**/
public with sharing class SendFilesAsAttachments {

    /**
    * @description 
    * @author Prashanth | 11-19-2021 
    **/
    @InvocableMethod
    public static void prepareRecordIds(List<requestWrapper> requests){
        List<Id> parentIds = new List<id>();
        List<Vet_Payments__c> tobeUpdateVetPays = new List<Vet_Payments__c>();
        List<Employer_Invoice__c> tobeUpdateEmpInvs = new List<Employer_Invoice__c>();
        Map<Id, Messaging.SingleEmailMessage> mailMap = new Map<Id, Messaging.SingleEmailMessage>();
        Map<Id, Account> idAccountMap = new Map<Id,Account>();
        Map<Id, Employer_Invoice__c> empInvsMap = new Map<Id,Employer_Invoice__c>();
        Map<Id, Vet_Payments__c> VetPayMap = new Map<Id,Vet_Payments__c>();

        if(requests[0].sendVetEmail){
            //Vet Payments
            List<Id> vetPayRecIds = new List<Id>();
            for(Vet_Payments__c vetPayRec : [SELECT Id, Name, Invoice_Subject__c, Vet__c, vet__r.Vet_Email__c, vet__r.Billing_Email_List__c FROM Vet_Payments__c WHERE Id IN :requests[0].vetPays]){
                idAccountMap.put(vetPayRec.Id, new Account(Id = vetPayRec.Vet__c, Vet_Email__c = vetPayRec.vet__r.Vet_Email__c, Billing_Email_List__c = vetPayRec.vet__r.Billing_Email_List__c));
                parentIds.add(vetPayRec.Id);    
                VetPayMap.put(vetPayRec.Id, vetPayRec);            
            }
        }
        else if(!requests[0].sendVetEmail){
            //Employer Invoices
            List<Id> empRecIds = new List<Id>();
            for(Employer_Invoice__c empInv : [SELECT Id, Name, Invoice_Subject__c, Employer__c, Employer__r.Billing_Email__c, Employer__r.Billing_Email_List__c FROM Employer_Invoice__c WHERE Id IN :requests[0].empInvs]){
                idAccountMap.put(empInv.Id, new Account(Id = empInv.Employer__c, Billing_Email__c = empInv.Employer__r.Billing_Email__c, Billing_Email_List__c = empInv.Employer__r.Billing_Email_List__c));
                parentIds.add(empInv.Id);
                empInvsMap.put(empInv.Id, empInv);
            }
        }

        mailMap.putAll(prepareEmailAttachments(parentIds));

        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();

        for(Id recId : mailMap.keySet()){
            Messaging.SingleEmailMessage mail = mailMap.get(recId);
            List<String>  toAddresses = new List<String>();
            if(recId.getSobjectType().getDescribe().getName().equalsIgnoreCase('Vet_Payments__c')){
                toAddresses.add(idAccountMap.get(recId).Vet_Email__c);
            }
            else if(recId.getSobjectType().getDescribe().getName().equalsIgnoreCase('Employer_Invoice__c')){
                toAddresses.add(idAccountMap.get(recId).Billing_Email__c);
            }
            mail.setToAddresses(toAddresses);
            Set<String> ccAddressesSet = new Set<String>();   
            List<String> ccAddressesList = new List<String>();
            if(String.isnotblank(idAccountMap.get(recId)?.Billing_Email_List__c)){
                idAccountMap.get(recId).Billing_Email_List__c = idAccountMap.get(recId)?.Billing_Email_List__c?.deleteWhitespace();
                ccAddressesList.addAll(idAccountMap.get(recId).Billing_Email_List__c.split(';'));
            }            
            
            for(String s : ccAddressesList){
                if(string.isNotBlank(s)){
                    ccAddressesSet.addall(s.split(','));
                }
            }
            
            if(!ccAddressesSet.isEmpty()){
                mail.setCcAddresses(new List<String> (ccAddressesSet));
            }
            mail.setOrgWideEmailAddressId(Label.InvoicingOrgEmailAddressId);
            //mail.setSubject('Test Email Please Ignore');
            
            if( (!mail.getToAddresses().isEmpty()) && mail.getToAddresses().size() > 0 && mail.getToAddresses()[0] != null){
                emails.add(mail);
                if(recId.getSobjectType().getDescribe().getName().equalsIgnoreCase('Vet_Payments__c')){
                    if(!Test.isRunningTest()){
                        mail.setHtmlBody(Messaging.renderStoredEmailTemplate(Label.InvoicingTemplateId, null, VetPayMap.get(recId).Vet__c).gethtmlbody());
                        mail.setPlainTextBody(Messaging.renderStoredEmailTemplate(Label.InvoicingTemplateId, null, VetPayMap.get(recId).Vet__c).getPlainTextBody());
                    }
                    mail.setSubject(VetPayMap.get(recId).Invoice_Subject__c);
                    tobeUpdateVetPays.add(new Vet_Payments__c(Invoice_Email_Sent__c = true, Id = recId));
                }
                if(recId.getSobjectType().getDescribe().getName().equalsIgnoreCase('Employer_Invoice__c')){
                    if(!Test.isRunningTest()){
                        mail.setHtmlBody(Messaging.renderStoredEmailTemplate(Label.InvoicingTemplateId, null, empInvsMap.get(recId).Employer__c).gethtmlbody());
                        mail.setPlainTextBody(Messaging.renderStoredEmailTemplate(Label.InvoicingTemplateId, null, empInvsMap.get(recId).Employer__c).getPlainTextBody());
                    }
                    mail.setSubject(empInvsMap.get(recId).Invoice_Subject__c);
                    tobeUpdateEmpInvs.add(new Employer_Invoice__c(Invoice_Email_Sent__c = true, Id = recId));
                
                }
            }
            if(String.isblank(mail.getSubject())){
                mail.setSubject('Invoice from UNITED PET CARE, LLC');
            }
        }  
        if(!Test.isRunningTest()){
            List<Messaging.SendEmailResult> emailResponses = Messaging.sendEmail(emails);
        }

        if(!tobeUpdateVetPays.isEmpty()){
            update tobeUpdateVetPays;
        }

        if(!tobeUpdateEmpInvs.isEmpty()){
            update tobeUpdateEmpInvs;
        }
        
    }

    /**
    * @description 
    * @author Prashanth | 11-16-2021 
    * @param List<Id> recordIds 
    **/
    private static Map<Id, Messaging.SingleEmailMessage> prepareEmailAttachments(List<Id> recordIds){
        Map<Id, List<String>> recordDocumentMap = new Map<Id, List<String>>();
        List<Id> conDocIds = new List<Id>();
        Map<Id, Id> conDocRecIdMap = new Map<Id,Id>();
        for(ContentDocumentLink cdl : [SELECT LinkedEntityId, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN :recordIds]){
            if(!recordDocumentMap.containsKey(cdl.LinkedEntityId)){
                recordDocumentMap.put(cdl.LinkedEntityId, new List<Id>{cdl.ContentDocumentId});
            }
            else{
                recordDocumentMap.get(cdl.LinkedEntityId).add(cdl.ContentDocumentId);
            }
            conDocRecIdMap.put(cdl.ContentDocumentId, cdl.LinkedEntityId);
            conDocIds.add(cdl.ContentDocumentId);
        }

        Map<Id, List<ContentVersion>> contDocMap = new Map<Id, List<ContentVersion>>();
        List<ContentVersion> versions = [SELECT Id, ContentDocumentId, Title, VersionData, FileExtension FROM ContentVersion WHERE ContentDocumentId IN :conDocIds AND IsLatest = true];
        for(ContentVersion cv : versions){
            if(!contDocMap.containsKey(cv.ContentDocumentId)){
                contDocMap.put(cv.ContentDocumentId, new List<ContentVersion>{cv});
            }
            else{
                contDocMap.get(cv.ContentDocumentId).add(cv);
            }
        }

        Map<Id, List<ContentVersion>> recCVMap = new Map<Id, List<ContentVersion>>();
        for(Id conDocId : conDocIds){
            if(!recCVMap.containsKey(conDocRecIdMap.get(conDocId))){
                List<ContentVersion> tempCvs = new List<ContentVersion>();
                tempCvs.addAll(contDocMap.get(conDocId));
                recCVMap.put(conDocRecIdMap.get(conDocId) , tempCvs);
            }
            else{
                List<ContentVersion> tempCvs = new List<ContentVersion>();
                tempCvs.addAll(contDocMap.get(conDocId));
                tempCvs.addAll(recCVMap.get(conDocRecIdMap.get(conDocId)));
                recCVMap.put(conDocRecIdMap.get(conDocId) , tempCvs);
            }
        }

        Map<Id, Messaging.SingleEmailMessage> emailMap = new Map<Id, Messaging.SingleEmailMessage>();
        for(Id recId : recCVMap.keySet()){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            List<Messaging.EmailFileAttachment> attachments = new List<Messaging.EmailFileAttachment>();
            for(ContentVersion cv : recCVMap.get(recId)){
                //Attachment classicAttach = new Attachment();
                //classicAttach.name = cv.Title+'.'+cv.FileExtension;
                //classicAttach.body = cv.versiondata;

                Messaging.EmailFileAttachment atchmnt = new Messaging.EmailFileAttachment();
                atchmnt.setFileName(cv.Title+'.'+cv.FileExtension);
                atchmnt.setBody(cv.VersionData);
                //atchmnt.setContentType(file.ContentType);
                attachments.add(atchmnt);
            }
            mail.setFileAttachments(attachments);
            emailMap.put(recId, mail);
        }

        return emailMap;
    }

    public class requestWrapper{
        @InvocableVariable
        public Boolean sendVetEmail = true;

        @InvocableVariable
        public List<Employer_Invoice__c> empInvs = new List<Employer_Invoice__c>();

        @InvocableVariable
        public List<Vet_Payments__c> vetPays = new List<Vet_Payments__c>();
    }
}