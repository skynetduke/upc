/**
 * @description       : Test class for test coverage on Pet trigger
 * @author            : Prashanth
 * @group             : 
 * @last modified on  : 11-05-2021
 * @last modified by  : Prashanth
 * Modifications Log
 * Ver   Date         Author      Modification
 * 1.0   11-05-2021   Prashanth   Initial Version
**/
@isTest
public without sharing class PetTriggerHandlerTest {
    @TestSetup
    static void makeData(){
        List<Account> accs = new List<Account>();
        String memberRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Member').getRecordTypeId();
        Account petOwner = new Account();
        petOwner.FirstName = 'Test FN';
        petOwner.LastName = 'Test LN';
        petOwner.recordTypeId = memberRecTypeId;
        accs.add(petOwner);

        String vetRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Vet').getRecordTypeId();
        Account vetOffice = new Account();
        vetOffice.Name = 'Test Vet Office';
        vetOffice.recordTypeId = vetRecTypeId;
        accs.add(vetOffice);

        Account secVetOffice = new Account();
        secVetOffice.Name = 'Secondary Vet Office';
        secVetOffice.recordTypeId = vetRecTypeId;
        accs.add(secVetOffice);
        insert accs;
    }

    @isTest
    public static void doTest(){
        Id petOwnerId;
        Id vetOfficeId;
        Id secVetOfficeId;
        for(Account acc : [SELECT Id,Name FROM Account WHERE NAME IN ('Test FN Test LN','Test Vet Office','Secondary Vet Office')]){
            if(acc.Name.equalsIgnoreCase('Test FN Test LN')){
                petOwnerId = acc.Id;
            }
            
            if(acc.Name.equalsIgnoreCase('Test Vet Office')){
                vetOfficeId = acc.Id;
            }

            if(acc.Name.equalsIgnoreCase('Secondary Vet Office')){
                secVetOfficeId = acc.Id;
            }
        }
        Test.startTest();
        
        List<Pet__c> pets = new List<Pet__c>();
        
        Pet__c pet = new Pet__c();
        pet.Name = 'Newton';
        pet.Pet_Owner__c = petOwnerId;
        pet.Vet_Office__c = vetOfficeId;
        pets.add(pet);

        Pet__c pet2 = new Pet__c();
        pet2.Name = 'Derek';
        pet2.Pet_Owner__c = petOwnerId;
        pet2.Vet_Office__c = vetOfficeId;
        pets.add(pet2);

        Pet__c pet3 = new Pet__c();
        pet3.Name = 'Derek';
        pet3.Pet_Owner__c = petOwnerId;
        pet3.Vet_Office__c = vetOfficeId;
        pet3.Deceased__c = true;
        //pets.add(pet3);
        insert pets;
        
        system.assertEquals(2, [SELECT Id, Number_Of_Pets_Calculate__c FROM Account WHERE Id = :vetOfficeId].Number_Of_Pets_Calculate__c);

        pet.Vet_Office__c = secVetOfficeId;
        update pet;

        system.assertEquals(1, [SELECT Id, Number_Of_Pets_Calculate__c FROM Account WHERE Id = :vetOfficeId].Number_Of_Pets_Calculate__c);
        system.assertEquals(1, [SELECT Id, Number_Of_Pets_Calculate__c FROM Account WHERE Id = :secVetOfficeId].Number_Of_Pets_Calculate__c);

        delete pet;
        system.assertEquals(0, [SELECT Id, Number_Of_Pets_Calculate__c FROM Account WHERE Id = :secVetOfficeId].Number_Of_Pets_Calculate__c);

        Test.stopTest();
    }
}