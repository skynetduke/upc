/**
 * @description       : Class is used to Generate Invoices from a flow and generate Employer rosters using S-docs
 * @author            : Prashanth
 * @group             : 
 * @last modified on  : 11-18-2021
 * @last modified by  : Prashanth
 * Modifications Log
 * Ver   Date         Author      Modification
 * 1.0   11-03-2021   Prashanth   Initial Version
**/
public without sharing class GenerateEmployerInvoices {
    /**
    * @description Generate Employer Invoices and Rosters
    * @author Prashanth | 11-03-2021 
    **/
    @InvocableMethod
    public static void generateInvoices(List<Date> invoiceDates){
        String recordTypeName = 'Employer';
        List<Account> employers = [SELECT Id, Total_Fee__c, Name FROM Account WHERE RecordType.DeveloperName = :recordTypeName AND Total_Fee__c > 0];
        List<Employer_Invoice__c> empInvs = new List<Employer_Invoice__c>();
        List<SDOC__SDJob__c> sdocJobs = new List<SDOC__SDJob__c>();

        
        for(Account emp : employers){
            Employer_Invoice__c empInv = new Employer_Invoice__c();
            empInv.Name = emp.Name+' - ' + String.valueOf(system.today()).removeEnd(' 00:00:00');
            empInv.Name = empInv.Name.substring(0,empInv.Name.length() >= 80 ? 79 : empInv.Name.length());
            empInv.Employer__c = emp.Id;
            empInv.Frequency__c = 'One-Time';
            empInv.Invoice_Date__c = (invoiceDates != null && !invoiceDates.isEmpty()) ? invoiceDates[0] : system.today();
            empInv.Invoice_Value__c = emp.Total_Fee__c;
            empInv.Schedule_Name__c = 'Schedule1';
            empInv.Status__c = 'Pending';
            empInvs.add(empInv);
        }

        //Generate Employer Invoices
        if(!empInvs.isEmpty()){
            insert empInvs;
        }

        for(Employer_Invoice__c empInv : empInvs){
            //Sdoc Job
            if(!Test.isRunningTest()){
                sdocJobs.add(new SDOC__SDJob__c(SDOC__Oid__c = empInv.Id,  SDOC__ObjAPIName__c = 'Employer_Invoice__c', SDOC__Doclist__c = Label.EmployerInvoiceRosterNames, SDOC__Start__c = true));
            }
        }

        //Generate Employer Roster and Change Roster
        if(!sdocJobs.isEmpty()){
            insert sdocJobs;
        }
    }
}