/**
 * @description       : 
 * @author            : Prashanth
 * @group             : 
 * @last modified on  : 11-24-2021
 * @last modified by  : Prashanth
 * Modifications Log
 * Ver   Date         Author      Modification
 * 1.0   11-24-2021   Prashanth   Initial Version
**/
public without sharing class UpdatePets implements Database.Batchable<sObject>{
    public Database.querylocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id FROM Pet__c WHERE Vet_Office__c <> null');
    }

    public void execute(Database.BatchableContext BC, List<sObject> scope){
        Set<Pet__c> pets = new Set<Pet__c>();
        List<AggregateResult> agrs = [SELECT Max(Id) id FROM Pet__c WHERE ID IN :scope GROUP BY Vet_Office__c];
        for(AggregateResult agr : agrs){
            String petId = String.valueOf(agr.get('id'));
            if(String.isNotBlank(petId)){
                pets.add(new Pet__c(Id = petId));
            }
        }

        List<Pet__c> petsList = new List<Pet__c>();
        petsList.addAll(pets);

        if(!petsList.isEmpty()){
            update petsList;
        }  
        
    }
    public void finish(Database.BatchableContext BC){
    
    }
}