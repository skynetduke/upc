/**
* @description       : 
* @author            : Prashanth
* @group             : 
* @last modified on  : 12-21-2021
* @last modified by  : Prashanth
* Modifications Log
* Ver   Date         Author      Modification
* 1.0   11-19-2021   Prashanth   Initial Version
**/
@isTest
public with sharing class AccountTriggerCustomTest {
    @isTest
    public static void doTest(){ 
        String vetRecId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Vet').getRecordTypeId();
        String empRecId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Employer').getRecordTypeId();
        String memRecId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Member').getRecordTypeId();
        List<Account> accs = new List<Account>();
        
        accs.add(generateAcc('Employer 1', empRecId));
        
        accs.add(generateAcc('Employer 2', empRecId));
        
        accs.add(generateAcc('Vet 1', vetRecId));

        accs.add(generateAcc('Vet 2', vetRecId));

        insert accs;

        Test.startTest();

        Account member1 = new Account(); 
        member1.FirstName = 'FName Mem 1';
        member1.LastName = 'LName Mem 1';
        member1.RecordTypeId = memRecId;
        member1.Employer__c = accs[0].Id;
        member1.Effective_Date__c = system.today();
        member1.Current_Vet__c = accs[2].Id;
        accs.add(member1);

        Account member2 = new Account(); 
        member2.FirstName = 'FName Mem 2';
        member2.LastName = 'LName Mem 2';
        member2.RecordTypeId = memRecId;
        member2.Employer__c = accs[0].Id;
        member2.Effective_Date__c = system.today().addDays(1);
        member2.Current_Vet__c = accs[2].Id;
        accs.add(member2);

        Account member3 = new Account(); 
        member3.FirstName = 'FName Mem 3';
        member3.LastName = 'LName Mem 3';
        member3.RecordTypeId = memRecId;
        member3.Employer__c = accs[1].Id;
        member3.Effective_Date__c = system.today();
        member3.Current_Vet__c = accs[3].Id;
        accs.add(member3);

        Account member4 = new Account(); 
        member4.FirstName = 'FName Mem 4';
        member4.LastName = 'LName Mem 4';
        member4.RecordTypeId = memRecId;
        member4.Employer__c = accs[1].Id;
        member4.Effective_Date__c = system.today().addDays(1);
        member4.Current_Vet__c = accs[3].Id;
        accs.add(member4);

        upsert accs Id;

        for(Account acc : [SELECT Id, Number_of_Registered_Members_Calculate__c FROM Account WHERE RecordType.DeveloperName = 'Vet' OR RecordType.DeveloperName = 'Employer']){
            System.assertEquals(2, acc.Number_of_Registered_Members_Calculate__c);
        }

        List<Account> mems = new List<Account>();
        for(Account acc : [SELECT Id From Account WHERE RecordType.DeveloperName = 'Member']){
            acc.Effective_Date__c = system.today().addMonths(1);
            mems.add(acc);
        }
        update mems;

        for(Account acc : [SELECT Id, Number_of_Registered_Members_Calculate__c FROM Account WHERE RecordType.DeveloperName = 'Vet' OR RecordType.DeveloperName = 'Employer']){
            System.assertEquals(2, acc.Number_of_Registered_Members_Calculate__c);
        }

        mems.clear();

        for(Account acc : [SELECT Id From Account WHERE RecordType.DeveloperName = 'Member']){
            acc.Effective_Date__c = system.today().addMonths(1);
            acc.Employer__c = [SELECT ID from Account WHERE Name = 'Employer 1' LIMIT 1]?.Id;
            acc.Current_Vet__c = [SELECT ID from Account WHERE Name = 'Vet 1' LIMIT 1]?.Id;
            mems.add(acc);
        }

        update mems;

        for(Account acc : [SELECT Id, Number_of_Registered_Members_Calculate__c FROM Account WHERE Name = 'Employer 1' OR Name = 'Vet 1']){
            System.assertEquals(4, acc.Number_of_Registered_Members_Calculate__c);
        }

        for(Account acc : [SELECT Id, Number_of_Registered_Members_Calculate__c FROM Account WHERE Name = 'Employer 2' OR Name = 'Vet 2']){
            System.assertEquals(0, acc.Number_of_Registered_Members_Calculate__c);
        }

        delete mems;

        for(Account acc : [SELECT Id, Number_of_Registered_Members_Calculate__c FROM Account WHERE RecordType.DeveloperName = 'Vet' OR RecordType.DeveloperName = 'Employer']){
            System.assertEquals(0, acc.Number_of_Registered_Members_Calculate__c);
        }

        Account member5 = new Account(); 
        member5.FirstName = 'FName Mem 5';
        member5.LastName = 'LName Mem 5';
        member5.RecordTypeId = memRecId;
        member5.Employer__c = accs[0].Id;
        member5.Member_Status__c = 'Cancelled';
        member5.Effective_Date__c = system.today().addDays(1);
        member5.Current_Vet__c = accs[2].Id;
        insert member5;

        for(Account acc : [SELECT Id, Number_of_Registered_Members_Calculate__c FROM Account WHERE Name = 'Employer 1' OR Name = 'Vet 1']){
            System.assertEquals(0, acc.Number_of_Registered_Members_Calculate__c);
        }

        Test.stopTest();
    }
    
    private static Account generateAcc(String Name, String recTypeId){ 
        Account acc = new Account(); 
        acc.Name = Name;
        acc.RecordTypeId = recTypeId;
        return acc;
    }
}