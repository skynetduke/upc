/**
 * @description       : Handler class to handle business logic
 * @author            : Prashanth
 * @group             : 
 * @last modified on  : 12-01-2021
 * @last modified by  : Prashanth
 * Modifications Log
 * Ver   Date         Author      Modification
 * 1.0   11-05-2021   Prashanth   Initial Version
**/

public without sharing class AccountTriggerHandler {
    /**
    * @description Apex method to calculate registered members of Employers, Vets
    * @author Prashanth | 11-05-2021 
    * @param Map<Id Account> newMap 
    * @param Map<Id Account> oldMap 
    **/
    public void calculateRegisteredMemberCount(Map<Id,Account> oldMap, Map<Id,Account> newMap){
        String memberRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Member').getRecordTypeId();
            Set<Id> accIds = new Set<Id>();
            
            if(oldMap != null && oldMap.size() > 0){
                for(Account acc : oldMap.values()){
                    //Check for Member accounts
                    if(String.valueof(acc.RecordTypeId).equalsignorecase(memberRecTypeId)){
                        if(String.isNotBlank(acc.Current_Vet__c)){
                            accIds.add(acc.Current_Vet__c);
                        }
                        if(String.isNotBlank(acc.Employer__c)){
                            accIds.add(acc.Employer__c);
                        }
                    }
                }
            }

            if(newMap != null && newMap.size() > 0){
                for(Account acc : newMap.values()){
                    //Check for Member accounts
                    if(String.valueof(acc.RecordTypeId).equalsignorecase(memberRecTypeId)){
                        if(String.isNotBlank(acc.Current_Vet__c)){
                            accIds.add(acc.Current_Vet__c);
                        }
                        if(String.isNotBlank(acc.Employer__c)){
                            accIds.add(acc.Employer__c);
                        }
                    }
                }
            }

            List<Account> accountsToBeUpdated = new List<Account>();
            for(Account acc : [SELECT Id, RecordType.DeveloperName, 
            (SELECT Id FROM Accounts2__r WHERE Registered_Member__c = TRUE), 
            (SELECT Id FROM Accounts3__r WHERE Registered_Member__c = TRUE) 
            FROM Account 
            WHERE Id IN :accIds])
            {//OR Effective_Date__c = NEXT_MONTH OR Effective_Date__c = NEXT_MONTH
                if(acc.RecordType.DeveloperName.equalsignorecase('Employer')){
                    Integer count = 0;
                    for(Account childAcc : acc.Accounts2__r){
                        count++;
                    }
                    accountsToBeUpdated.add(new Account(Id = acc.Id, Number_of_Registered_Members_Calculate__c = count));
                }
                if(acc.RecordType.DeveloperName.equalsignorecase('Vet')){
                    Integer count = 0;
                    for(Account childAcc : acc.Accounts3__r){
                        count++;
                    }
                    accountsToBeUpdated.add(new Account(Id = acc.Id, Number_of_Registered_Members_Calculate__c = count));
                }
            }

            if(!accountsToBeUpdated.isEmpty()){
                update accountsToBeUpdated;
            }
        }
}